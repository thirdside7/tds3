// Copyright Epic Games, Inc. All Rights Reserved.



using UnrealBuildTool;

public class TPS : ModuleRules
{
	public TPS(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        //PublicIncludePaths.AddRange(new string[]
        //{
        //    "TSP/BaseCatalog",
        //    "TSP/Character",
        //    "TSP/FuncLibrary"
        //});

        PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine",
                                                            "InputCore", "HeadMountedDisplay", 
                                                            "NavigationSystem", "AIModule",
                                                            "PhtsicsCore", "Slate" });
    }
}
